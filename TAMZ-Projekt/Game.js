import Alien from "./Alien.js";
import GameObject from "./GameObject.js";
import Player from "./Player.js";
import Projectile from "./Projectile.js";
import Controls from "./Controls.js";
import Barricade from "./Barricade.js";

const ALIEN_SPEED = 2;
const RED_SPAWN = 15000;

export default class Game{

    constructor(){
        this.gameObjects=[];
        this.cWidth = 800;
        this.cHeight = 600;
        this.index = 0;        
        this.score = 0;
        this.canvas = document.getElementById("gameCanvas"); 
        this.playerName = localStorage.getItem("playerName");
        this.ctx = this.canvas.getContext("2d");
        this.player = new Player(10, 240);
        this.projectiles = [];
        this.aliens = [];
        this.oldDelta = 0;
        this.alienMove = ALIEN_SPEED;
        this.alienSpeed = 1;
        this.controls = new Controls(this.player);
        this.ctx.imageSmoothingEnabled = false;
        this.redCountdown = RED_SPAWN;


        this.cWidth = this.canvas.width;
        this.cHeight = this.canvas.height;

        for( let x = 80; x < this.canvas.width-105; x += 25){
            this.aliens.push(new Alien(x, 40, 0));
        }
        for( let x = 50; x < this.canvas.width-75; x += 25){
            this.aliens.push(new Alien(x, 60, 1));
        }
        for( let x = 20; x < this.canvas.width-45; x += 25){
            this.aliens.push(new Alien(x, 80, 2));
        }
        
        this.barricades = [];
        for( let x = 10; x < this.canvas.width-16; x += 44){
            this.barricades.push(new Barricade(x, 220));
        }
    }

    gameLoop() {
        requestAnimationFrame((d)=>{
            if(this.aliens.length == 0){
                this.gameOver();
                return;
            }
            let delta = d -this.oldDelta;

            this.redCountdown -= delta;
            if(this.redCountdown <= 0){
                this.aliens.push(new Alien( 1, 20, 3));
                this.redCountdown = Math.random() * RED_SPAWN;
            }
            this.ctx.clearRect(0, 0, this.cWidth, this.cHeight);
            this.ctx.fillstyle = 'whitesmoke';
            this.player.render(this.ctx);
            document.getElementById("displayName").innerHTML = this.playerName;
            document.getElementById("displayScore").innerHTML = "Score: " + this.score;


            let swap = false;
            for(let barricade of this.barricades){
                barricade.render(this.ctx);
            }
            for(let alien of this.aliens){
                alien.render(this.ctx, delta/400);
                if(Math.random()*10 >= 9.9){
                    if(this.projectiles.length < 6) this.projectiles.push(new Projectile(alien.x+alien.width/2, alien.y+alien.height, 1));
                }
                for(let projectile of this.player.projectiles){
                    if(projectile.intersects(alien) && alien.health === 3){
                        this.score += 50;
                        if(alien.type === 3)this.score += 100;
                        alien.health = 2;
                        projectile.y = 0;
                    }
                }
                if(this.alienMove < 0 && alien.type < 3){
                    
                    alien.x += this.alienSpeed;
                    if(alien.x+alien.width > this.cWidth || alien.x <= 0){
                        swap = true;
                    }
                }
                if(alien.type === 3){
                    alien.x++;
                }
                
            }
            for(let projectile of this.projectiles){
                projectile.render(this.ctx);
                if(projectile.intersects(this.player)){
                    this.player.getHit();
                    projectile.y = 1000;
                    if(this.player.lives < 0){
                    this.gameOver();  
                    return;                                      
                    }
                }
                for(let barricade of this.barricades){
                    if(projectile.intersects(barricade)){
                        barricade.health++;
                        projectile.y = 1000;
                    }
                }
            }
            
            if(swap){
                for(let alien of this.aliens){
                    alien.y += 2;
                }
                this.alienSpeed *= -1;
            }
            this.projectiles = this.projectiles.filter(i => i.y < this.cHeight);
            this.aliens = this.aliens.filter( i => i.health > 0 && i.x >= 0 && i.x < this.cWidth);
            this.barricades = this.barricades.filter(i => i.health < 7);
            this.oldDelta = d;
            if(this.alienMove < 0)this.alienMove = ALIEN_SPEED * this.aliens.length;
            this.alienMove -= delta;
            this.gameLoop();
        });
        
    }

    gameOver(){
        let scores = localStorage.getItem("scores");
        if(!scores) {
            scores = [];
        }else{
            scores = JSON.parse(scores);
        }
        scores.push({name:this.playerName,score: this.score});

        localStorage.setItem("scores", JSON.stringify(scores));

        location.href="/game/hiscores.html";
    }

}
