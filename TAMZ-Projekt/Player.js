import GameObject from "./GameObject.js";
import Projectile from "./Projectile.js";

export default class Player extends GameObject{
    
    constructor(x, y){
        super(x, y, ['textures/favicon.png'], 16, 8);
        this.left = 0;
        this.right = 0;
        this.projectiles = [];
        this.lives = 3;
    }
    render(ctx){
        if(this.x <= 0)this.left = 0;
        if(this.x >= ctx.canvas.width - this.width)this.right = 0;
        this.x += this.right - this.left;
        super.render(ctx);
        this.projectiles = this.projectiles.filter(i => i.y > 0);
        for(let projectile of this.projectiles){
            projectile.render(ctx);
        }
        
    }

    getHit(){
        this.lives--;
        if(this.lives <= 2){
            document.getElementById("life2").style.display = "none";
        }
        if(this.lives <= 1){
            document.getElementById("life1").style.display = "none";
        }
        if(this.lives <= 0){
            document.getElementById("life0").style.display = "none";
        }
    }

    shoot(){
        if(this.projectiles.length > 2)return;
        this.projectiles.push(new Projectile(this.x + this.width/2, this.y));
    }
}