import Game from './Game.js';

const music = document.getElementById("stopMusic");
const audio = new Audio("Noise-Long-Version-chosic.com_.mp3");
audio.play();
music.onclick = ()=>{
    if(music.src.split("/")[4] == "mute.png"){
        music.src = "textures/volume.png";
        audio.play();
    }else{
        music.src = "textures/mute.png";
        audio.pause();
    }
    
};

const game = new Game();
game.gameLoop();

