
export default class Controls{
    constructor(player){
        this.player = player;
        this.leftButton = document.getElementById("leftButton");
        this.rightButton = document.getElementById("rightButton");
        this.spaceBar = document.getElementById("spaceButton");

        this.leftButton.addEventListener('contextmenu', event => event.preventDefault());
        this.rightButton.addEventListener('contextmenu', event => event.preventDefault());
        this.spaceBar.addEventListener('contextmenu', event => event.preventDefault());
        this.leftButton.addEventListener("touchstart", ()=>{
            this.player.left = 1;
        });
        this.leftButton.addEventListener("touchend", ()=>{
            this.player.left = 0;
        });
        this.rightButton.addEventListener("touchstart", ()=> {
            this.player.right = 1;
        });
        this.rightButton.addEventListener("touchend", ()=> {
            this.player.right = 0;
        });
        this.spaceBar.addEventListener("touchstart", () => {
            this.player.shoot();
        });
        document.addEventListener("keydown", (ev)=>{
            if(ev.key === "ArrowRight"){
                this.player.right = 1;
            }
            if(ev.key === "ArrowLeft"){
                this.player.left = 1;
            }
            if(ev.key == " "){
                this.player.shoot();
            }
        });
        document.addEventListener("keyup", (ev)=>{
            switch(ev.key){
                case "ArrowLeft":
                    this.player.left = 0;
                break;
                case "ArrowRight":
                    this.player.right = 0;
                break;
            }
        });
        
    }


}
