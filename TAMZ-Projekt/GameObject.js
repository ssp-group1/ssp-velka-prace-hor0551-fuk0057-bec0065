export default class GameObject{
    constructor(x, y, textures=['textures/emzak_1A.png', 'textures/emzak_1B.png'], width=100, height=100){
        this.x = x;
        this.y = y;
        this.textures = textures.map(i => new Image(width, height));
        for(let i in this.textures){
            this.textures[i].src = textures[i];
            this.textures[i].width = width;
            this.textures[i].height = height;
        }
        this.width = width;
        this.height = height;
        this.textureIndex = 0;
    }

    render(ctx){
        if(this.textureIndex >= this.textures.length){
            this.textureIndex = 0;
        }
        ctx.drawImage(this.textures[Math.floor(this.textureIndex)], this.x, this.y, this.width, this.height);
        
        this.textureIndex += 0.01;
    }
    
    intersects(other){
        if(this.x > other.x && this.x < other.x+other.width && this.y > other.y && this.y < other.y+other.height){
            return true;
        }
        if(this.x+this.width > other.x && this.x+this.width < other.x+other.width && this.y > other.y && this.y < other.y+other.height){
            return true;
        }
        if(this.x > other.x && this.x < other.x+other.width && this.y+this.height > other.y && this.y+this.height < other.y+other.height){
            return true;
        }
        if(this.x+this.width > other.x && this.x+this.width < other.x+other.width && this.y+this.height > other.y && this.y+this.height < other.y+other.height){
            return true;
        }

        return false;
    }

}