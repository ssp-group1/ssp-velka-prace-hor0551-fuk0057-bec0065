import GameObject from "./GameObject.js";

const BARRICADES = [
    "textures/bariera_1.png",
    "textures/bariera_2.png",
    "textures/bariera_3.png",
    "textures/bariera_4.png",
    "textures/bariera_5.png",
    "textures/bariera_6.png",
    "textures/bariera_7.png"
];


export default class Barricade extends GameObject{
    constructor(x,y){
        super(x,y, ["textures/bariera_1.png"], 20, 12);
        this.health = 0;
    }
    render(ctx){
        this.textures[0].src = BARRICADES[this.health] || BARRICADES[6];
        super.render(ctx);
    }
}