

onload = ()=>{
    const scores = JSON.parse(localStorage.getItem("scores"));


    const body = document.getElementById("scores");

    for(let score of scores.sort((a,b) => {
        return Number(b.score) - Number(a.score);
    }).slice(0,10)){
        body.innerHTML += `<tr> <td>${score.name}</td><td>:</td><td>${score.score}</td> </tr>`;
    }
};