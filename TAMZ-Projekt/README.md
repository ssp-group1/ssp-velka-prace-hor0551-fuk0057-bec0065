# :space_invader: Space Invaders Projekt

![GamePreview](screenshot.png)

Tuto hru pro prohlížeč jsem vytvořila jako semestrální práci do předmětu __Tvorba aplikací pro mobilní zařízení I (TAMZ I)__ <br>

**Použité jazyky:** JavaScript, HTML5, CSS3

## :video_game: Hru si můžete vyzkoušet [zde](https://ssp-velka-prace-hor0551-fuk0057-bec0065-ssp-grou-3f7fc674edf73b.gitlab.io/game/IntroScreen.html)<br>

![Controls](jakHtrat.png)

---

**Vytvořila:** Fukalová Eva, studentka 2. ročníku informatiky na VŠB – Technická univerzita Ostrava, Fakulta elektrotechniky a informatiky<br>
**Vytvořeno :** jaro 2024
