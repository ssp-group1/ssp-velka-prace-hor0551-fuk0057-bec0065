import GameObject from "./GameObject.js";

export default class Projectile extends GameObject{
    constructor(x, y, speed = -1){
        
        super(x, y, [], 2, 10);
        this.speed = speed;
    }
    render(ctx){
        this.y += this.speed;
        ctx.fillStyle = 'white';
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }
}