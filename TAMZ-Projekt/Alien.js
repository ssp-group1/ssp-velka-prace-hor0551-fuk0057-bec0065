import GameObject from "./GameObject.js";

//3 
const ALIEN_TYPES = [
    ["textures/emzak_1A.png", "textures/emzak_1B.png"],
    ["textures/emzak_2A.png", "textures/emzak_2B.png"],
    ["textures/emzak_3A.png", "textures/emzak_3B.png"],
    ["textures/cerveny_emzak.png"]
];

const dead = new Image(16, 8);
//explodujici emzak pri smrti
dead.src = "textures/smrt_emzaka.png";

export default class Alien extends GameObject{
    constructor(x,y, type){
        super(x,y, ALIEN_TYPES[type], 16, 14);
        this.health = 3;
        this.type = type;
    }

    render(ctx, delta){
        if(this.health <= 2){
            this.health -= delta;
            this.textures = [dead];
        }
        super.render(ctx);
    }
}