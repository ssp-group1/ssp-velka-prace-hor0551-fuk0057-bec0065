# Velký projekt do předmětu SSP

Tento repozitář je výsledkem naší společné závěrečné práce do předmětu
Správa softwarových projektů ve zkratce SSP. Naším cílem bylo splnit
všechny povinné části zadání a také nepovinné, které byly
u našeho projektu možné. Níže pak lze vidět, které jsme také i splnili.

Projekt navíc sám o sobě navazuje na "Malý projekt", který jsme měli
taktéž v rámci splnění daného předmětu vytvořit. Odkaz na tento druhý
projekt se nachází [zde](https://gitlab.com/ssp-group1/ssp-mala-prace-fuk0057-hor0551-bec0065).

> :memo: __Odkaz na hru:__ Hru samotnou si můžete zahrát na adrese, která se
nachází [zde](https://ssp-velka-prace-hor0551-fuk0057-bec0065-ssp-grou-3f7fc674edf73b.gitlab.io/game/IntroScreen.html).

## __Obecné informace__

__Předmět:__ SSP

__Datum vypracování:__ 05.06.2024

__Autoři projektu:__ FUK0057, HOR0551, BEC0065

## __Detailní informace__

__Název projektu:__ Space Invaders pro prohlížeče

__Typ projektu:__ Hra

__Použity programovací jazyky:__ JavaScript

__Použity kódovací jazyky:__ HTML5, CSS3, Markdown

![GamePreview](./TAMZ-Projekt/screenshot.png)

## Splněná zadání

> Závěrečná práce vychází z "malé" práce. Použijte svůj projekt, na kterém
pracujete a dejte jej do GIT repozitáře a pokuste se připravit kontinuální
integraci svého projektu. V závislosti na použitém programovacím jazyce či
sadě programovacích jazyků navhrněte a vytvořte vhodné úkoly ve vaší
kontinuální integraci. Pro úlohy v CI použijte vhodné Docker kontejnery,
případně vytvořte vlastní. Pokud povaha CI vyžaduje, rozdělte úlohy (jobs)
do vhodných fází (stages).
> Požadavky na kvalitu práce (hodnotící kritéria):
>
> - [x] GIT repozitář na Gitlabu.
> - [x] Repozitář má README.md (K čemu projekt slouží, jak se používá, jak
se instaluje, jaké jsou prerekvizity, apod.)
> - [x] Použití vlastního projektu (ideální kandidát má kombinaci jazyků -
např. Python a BASH, Markdown a JS, apod.)
> - [x] Vytvořená CI v repozitáři.
> - [x] CI má minimálně tři úlohy:
>    - [x] Test kvality Markdown stránek. (Kvalita dokumentace je důležitá.
Minimálně README.md by mělo být v repozitáři dostupné.)
>    - [x] Kontrola syntaxe každého jazyka.
>    - [x] Použití "lint" nástroje pro každý jazyk.
> - [ ] (Volitelné/doporučené) Nástroj na kontrolu dostupnosti aktualizací/
security updates pro moduly jazyka (pokud existuje).
> - [ ] (Volitelné/doporučené) Nástroj na kontrolu testů/code coverage
(pokud existuje).
> - [x] (Volitelné/doporučené) Pokud některé nástroje umí generovat HTML
reporty - umistěte je na Gitlab pages.
> - [x] (Volitelné/doporučené) Repozitář obsahuje šablonu ticketu.

---

> :memo: __Poznámka:__ U jedné z HTML stránek jsme zanechali chybu. Tato
chyba nám slouží pro reprezentaci funkčnosti volitelné úlohy a to generování
reportů do [HTML](https://ssp-velka-prace-hor0551-fuk0057-bec0065-ssp-grou-3f7fc674edf73b.gitlab.io/).
